// ============================================================================
// Rocks'n'Diamonds - McDuffin Strikes Back!
// ----------------------------------------------------------------------------
// (c) 1995-2014 by Artsoft Entertainment
//                  Holger Schemel
//                  info@artsoft.org
//                  https://www.artsoft.org/
// ----------------------------------------------------------------------------
// conf_mus.h
// ============================================================================

// ------- this file was automatically generated -- do not edit by hand -------

#ifndef CONF_MUS_H
#define CONF_MUS_H

// values for music configuration

#define MUS_BACKGROUND					0
#define MUS_BACKGROUND_TITLE_INITIAL			1
#define MUS_BACKGROUND_TITLE				2
#define MUS_BACKGROUND_MAIN				3
#define MUS_BACKGROUND_NAMES				4
#define MUS_BACKGROUND_LEVELS				5
#define MUS_BACKGROUND_LEVELNR				6
#define MUS_BACKGROUND_SCORES				7
#define MUS_BACKGROUND_EDITOR				8
#define MUS_BACKGROUND_INFO				9
#define MUS_BACKGROUND_SETUP				10
#define MUS_BACKGROUND_TITLESCREEN_INITIAL_1		11
#define MUS_BACKGROUND_TITLESCREEN_INITIAL_2		12
#define MUS_BACKGROUND_TITLESCREEN_INITIAL_3		13
#define MUS_BACKGROUND_TITLESCREEN_INITIAL_4		14
#define MUS_BACKGROUND_TITLESCREEN_INITIAL_5		15
#define MUS_BACKGROUND_TITLESCREEN_1			16
#define MUS_BACKGROUND_TITLESCREEN_2			17
#define MUS_BACKGROUND_TITLESCREEN_3			18
#define MUS_BACKGROUND_TITLESCREEN_4			19
#define MUS_BACKGROUND_TITLESCREEN_5			20
#define MUS_BACKGROUND_TITLEMESSAGE_INITIAL_1		21
#define MUS_BACKGROUND_TITLEMESSAGE_INITIAL_2		22
#define MUS_BACKGROUND_TITLEMESSAGE_INITIAL_3		23
#define MUS_BACKGROUND_TITLEMESSAGE_INITIAL_4		24
#define MUS_BACKGROUND_TITLEMESSAGE_INITIAL_5		25
#define MUS_BACKGROUND_TITLEMESSAGE_1			26
#define MUS_BACKGROUND_TITLEMESSAGE_2			27
#define MUS_BACKGROUND_TITLEMESSAGE_3			28
#define MUS_BACKGROUND_TITLEMESSAGE_4			29
#define MUS_BACKGROUND_TITLEMESSAGE_5			30

#define NUM_MUSIC_FILES					31

#endif	// CONF_MUS_H
