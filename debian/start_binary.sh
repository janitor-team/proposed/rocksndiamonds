#!/bin/sh

LEVEL_DIR=/usr/share/games/rocksndiamonds

if test -d $LEVEL_DIR/levels; then
	exec /usr/games/rocksndiamonds-bin "$@"
else
	echo "Game data not installed!"
	echo
	echo "Run \`dpkg-reconfigure rocksndiamonds' as root"
	echo "to install/update game levels."
fi
